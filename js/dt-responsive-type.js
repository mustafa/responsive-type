// protecting the $ :)
(function ($) {

	$.fn.ResponsiveType = function ( options ) {
		// Contstants
		var GOLDERRATIO = 1.61803398875;
		var container = this;

		var settings = $.extend({
			h1: null,
			h2: null,
			h3: null,
			h4: null,
			p:  null,
			cpl:  null,
			small: null,
			minsize: null,
			default: false,
		}, options);

		// Get container width 
		function getContainerWidth(){
			return container.width();
		}

		function getCPL () {
			var charactersPerLine = settings.cpl === null ? 70 : settings.cpl;
			return charactersPerLine;
		}

		function getFontSizes() {
			var pSize = settings.p === null ? getGoldenRatioScale().p : settings.p;
			var smallSize = settings.small === null ? getGoldenRatioScale().small : settings.small;
			var h1Size = settings.h1 === null ? getGoldenRatioScale().h1 : settings.h1;
			var h2Size = settings.h2 === null ? getGoldenRatioScale().h2 : settings.h2;
			var h3Size = settings.h3 === null ? getGoldenRatioScale().h3 : settings.h3;
			var h4Size = settings.h4 === null ? getGoldenRatioScale().h4 : settings.h4;

			return { p:pSize, small:smallSize, h4:h4Size, h3:h3Size, h2:h2Size, h1:h1Size };
		}

		// getGoldenRatioScale font sizes 
		function getGoldenRatioScale () {
			
			// This was the old value that i was using that didnt take CPL in to account
			// var bodyCopy = Math.round(Math.sqrt(getContainerWidth())/GOLDERRATIO);			
			var firstGoldenScale = Math.round(getContainerWidth()/(getCPL() / 1.9));

			var bodyCopy = settings.minsize > firstGoldenScale ? settings.minsize: firstGoldenScale;

			var footnotes = Math.round((bodyCopy/2) * GOLDERRATIO);

			var smallSubtitles = Math.round(bodyCopy + 4);
			var subtitles = Math.round(bodyCopy * GOLDERRATIO);
			var titles = Math.round(subtitles * GOLDERRATIO);
			var headers = Math.round(titles * GOLDERRATIO);

			var defaults = bodyCopy;

			return { h1:headers, h2:titles, h3:subtitles, h4:smallSubtitles, p:bodyCopy, default: defaults, small:footnotes };
		}

		// Set font sizes 
		function setFontSizes () {
			
			$("h1", container).css({fontSize: getFontSizes().h1 });
			$("h2", container).css({fontSize: getFontSizes().h2 });
			$("h3", container).css({fontSize: getFontSizes().h3 });
			$("h4", container).css({fontSize: getFontSizes().h4 });
			$("p", container).css({fontSize: getFontSizes().p });
			$("small", container).css({fontSize: getFontSizes().small });
			
			setLineHeight();

			if (settings.default === true) {
				$(container).css({fontSize: getGoldenRatioScale().default });
			}
		}

		function setLineHeight () {
			$("h1", container).css({ lineHeight: getFontSizes().h1+"px"});
			$("h2", container).css({  lineHeight: getFontSizes().h1+"px"});
			$("h3", container).css({ lineHeight: getFontSizes().h2+"px"});
			$("h4", container).css({ lineHeight: getFontSizes().h3+"px"});
			$("p", container).css({ lineHeight: getFontSizes().h3+"px"});
			$("small", container).css({ lineHeight: getFontSizes().p+"px"});
		}

		/// On browser window resize
		var windowResized = ($(window).resize(function () {
			waitForFinalEvent(function(){
				setFontSizes();
				// console.log("called waitForFinalEvent from windowResized");
			}, 500, "a unique string?");
		}));

		// Timer to wait for the final event call < taken from stackedOverflow
		var waitForFinalEvent = (function () {
			var timer = {};
			return function (callback, ms, uniqueID) {
				if (!uniqueID) {
					uniqueID = "Don't call this twice without uniqueID";
				}
				if (timer[uniqueID]) {
					clearTimeout (timer[uniqueID]);
				}
				timer[uniqueID] = setTimeout(callback, ms);
			};
		})();

		setFontSizes();

		// This allows chainning.
		return this;
	};
}(jQuery));

