## Responsive-Type ##

This is an experiment that explores responsive type on the web. Currently it is quite difficult to automatically adjust the size of paragraph copy based on  characters per line (CPL), the width of the column and the ratio between elements.

We do have css viewport unit types vh, vw, vmin and vmax which will change the size of an element based on a percentage of port height or width. Though this can be problematic as viewport units are currently buggy in Webkit browsers and they don't solve the question of CPL or the ratio between text elements.

Responsive-Type is a jQuery plug-in that sets the font-size of the <P> tags according to CPL and column width. By default the CPL is set to 70 though this can be changed as desired. That way we can make the column width fluid and not worry about the font size.

In terms of type scale between elements, Responsive-Type uses the Golden Ratio to give visual hierarchy. The maths involved is based on the wonderful work by [Chris Pearson](http://www.pearsonified.com/2011/12/golden-ratio-typography.php). 

This is still work in progress :) but you can see [a demo here](http://cdpn.io/gDieb).

## The API ##
The tags that are scaled to the Golden Ratio are; 

```
<H1>
<H2>
<H3>
<H4>
<P>
<SMALL>
```

### Basic implementation ###
```
$("#container").ResponsiveType();
```

### Fix element font sizes ###
To prevent the resizing of certain elements simply give the element a font size: 

```
$("#container").ResponsiveType({h1: "30", h2: "20", h3: "auto"});
```

### Set all other elements ###
By default other tag elements do not have their sizes set but you can set them too: 

```
default:true
```

### Set CPL of the <P> ###
The default CPL is 70 though we can set this; 

```
$("#container").ResponsiveType({cpl: 50});
```

### Set Minimum font size <P> ###

The font size is limitless, which could cause scaling issues on very small devices. We can set a minimum font size for the main body copy;

```
$("#container").ResponsiveType({minsize: 14});
```


### TODO ###
# Max font size.
# Fix lineheight bugs when h tags have their sizes fixed. 